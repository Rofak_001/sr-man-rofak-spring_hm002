package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Controller;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Article;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("restTemplate")
public class MyRestController {
    private final String url = "http://localhost:8080/api/v1/";
    private RestTemplate restTemplate;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    //TODO Category
    @GetMapping("/category")
    public ResponseEntity<String> getAll() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> result = restTemplate.exchange(url + "category", HttpMethod.GET, httpEntity, String.class);
        return result;
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<String> getOne(@PathVariable int id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> result = restTemplate.exchange(url + "category/" + id, HttpMethod.GET, httpEntity, String.class);
        return result;
    }

    @PostMapping("/category")
    public ResponseEntity<String> save(@RequestBody Category category) {
        ResponseEntity responseEntity = restTemplate.postForEntity(url + "category", category, String.class);
        return responseEntity;
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<String> update(@RequestBody Category category, @PathVariable int id) {
        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(id));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Category> httpEntity = new HttpEntity<>(category, httpHeaders);
        ResponseEntity<String> rs = restTemplate.exchange(url + "category/{id}", HttpMethod.PUT, httpEntity, String.class, map);
        return rs;
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", String.valueOf(id));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url + "category/{id}", HttpMethod.DELETE, httpEntity, String.class, map);
        return responseEntity;
    }

    //TODO Articles
    @GetMapping("/article")
    public ResponseEntity<String> getAllArticle(@RequestParam(required = false) String categoryTitle) {
        Map<String, String> map = new HashMap<>();
        map.put("title", categoryTitle);
        if (categoryTitle != null) {
            return restTemplate.getForEntity(url + "article?categoryTitle={title}", String.class, map);
        } else {
            return restTemplate.getForEntity(url + "article", String.class);
        }
    }
    @GetMapping("/article/{id}")
    public ResponseEntity<String> getOneArticleById(@PathVariable int id){
        Map<String,String> map=new HashMap<>();
        map.put("id",String.valueOf(id));
        return restTemplate.getForEntity(url+"article/{id}",String.class,map);
    }
    @PostMapping("/article")
    public ResponseEntity<String> saveArticle(@RequestBody Article article){
        return restTemplate.postForEntity(url+"article",article,String.class);
    }
    @PutMapping("/article/{id}")
    public ResponseEntity<String> updateArticle(@RequestBody Article article,@PathVariable int id){
        HttpHeaders httpHeaders=new HttpHeaders();
        Map<String,String> param=new HashMap<>();
        param.put("id",String.valueOf(id));
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Article> entity=new HttpEntity<>(article,httpHeaders);
        return restTemplate.exchange(url+"article/{id}",HttpMethod.PUT,entity,String.class,param);
    }
    @DeleteMapping("/article/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable int id){
        Map<String, String> param=new HashMap<>();
        param.put("id",String.valueOf(id));
        HttpHeaders httpHeaders=new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity entity=new HttpEntity(httpHeaders);
       return restTemplate.exchange(url+"article/{id}",HttpMethod.DELETE,entity,String.class,param);
    }
}
