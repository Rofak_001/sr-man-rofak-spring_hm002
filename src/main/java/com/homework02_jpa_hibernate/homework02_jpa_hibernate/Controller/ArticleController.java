package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Controller;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Article;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ArticleController {
    private ArticleService articleService;
    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }
    @GetMapping("/article")
    public ResponseEntity<Map<String,Object>> getAll(@RequestParam(required = false) String categoryTitle){
        if(categoryTitle!=null){
            return ResponseEntity.ok(articleService.getArticleByCategoryTitle(categoryTitle));
        }else{
            return ResponseEntity.ok(articleService.getAll());
        }
    }
    @GetMapping("/article/{id}")
    public ResponseEntity<Map<String,Object>> getOne(@PathVariable int id){
        return ResponseEntity.ok(articleService.getOne(id));
    }
    @PostMapping("/article")
    public ResponseEntity<Map<String,Object>>save(@RequestBody Article article){
        return ResponseEntity.ok(articleService.save(article));
    }
    @PutMapping("/article/{id}")
    public  ResponseEntity<Map<String,Object>> update(@RequestBody Article article,@PathVariable int id){
        return ResponseEntity.ok(articleService.update(article,id));
    }
    @DeleteMapping("/article/{id}")
    public ResponseEntity<Map<String,Object>> deleted(@PathVariable int id){
        return ResponseEntity.ok(articleService.delete(id));
    }
}
