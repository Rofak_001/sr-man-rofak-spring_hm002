package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Controller;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class CategoryController {
    private CategoryServices categoryServices;

    @Autowired
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
    }
    @GetMapping("/category")
    public ResponseEntity< Map<String,Object>> getALl(){
        return ResponseEntity.ok(categoryServices.getAll());
    }
    @GetMapping("/category/{id}")
    public ResponseEntity<Map<String,Object>>getOne(@PathVariable int id){
        Map<String,Object> map=new HashMap<>();
        if(categoryServices.getOne(id)!=null){
            map.put("data",categoryServices.getOne(id));
        }else{
            map.put("Message","Not found!");
        }
        return ResponseEntity.ok(map);

    }
    @PostMapping("/category")
    public ResponseEntity<Map<String,Object>> save(@RequestBody Category category){
        Map<String,Object> map=new HashMap<>();
        map.put("Message","Save Successfully");
        map.put("data:",category);
        categoryServices.save(category);
        return ResponseEntity.ok(map);
    }
    @PutMapping("/category/{id}")
    public ResponseEntity<Map<String,Object>> updated(@RequestBody Category category,@PathVariable int id){
        return ResponseEntity.ok(categoryServices.update(category,id));
    }
    @DeleteMapping("/category/{id}")
    public ResponseEntity<Map<String,Object>> deleted(@PathVariable int id){
        return ResponseEntity.ok(categoryServices.delete(id));
    }
}
