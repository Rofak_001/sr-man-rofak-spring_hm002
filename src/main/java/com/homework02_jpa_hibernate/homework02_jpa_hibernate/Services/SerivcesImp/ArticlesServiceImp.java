package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.SerivcesImp;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Article;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Repository.ArticleRepository;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Repository.CategoryRepository;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticlesServiceImp implements ArticleService {
    private ArticleRepository articleRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public Map<String, Object> getAll() {
        List<Article> list=articleRepository.getAll();
        Map<String,Object> map=new HashMap<>();
        if(!list.isEmpty()){
            map.put("data",list);
        }else
        {
            map.put("Message","No Data!");
        }
        return map;

    }

    @Override
    public Map<String, Object> getArticleByCategoryTitle(String categoryTitle) {
        Map<String, Object> map = new HashMap<>();
        List<Article> list = articleRepository.getArticleByCategoryTitle(categoryTitle);
        if (!list.isEmpty()) {
            map.put("data", list);
        } else {
            map.put("Message", "data Not found!");
        }
        return map;
    }

    @Override
    public Map<String, Object> getOne(int id) {
        Map<String, Object> map = new HashMap<>();
        if (articleRepository.getOne(id) != null) {
            map.put("data", articleRepository.getOne(id));
        } else {
            map.put("Message", "Data not Found!");
        }
        return map;
    }

    @Override
    public  Map<String, Object> update(Article article, int id) {
        Map<String,Object>map=new HashMap<>();
        Category category=categoryRepository.getOne(article.getCategory().getId());
//        Article article1=;
        if(articleRepository.getOne(id)!=null){
            if(category!=null){
                if(articleRepository.Update(article,id)!=0){
                    map.put("Message","Updated Successfully!");
//                    article.setCategory(category);
//                    map.put("data",article);
                }
            }else{
                map.put("Message","Category id "+article.getCategory().getId()+" Not found!");
            }
        }else{
            map.put("Message","id "+id+" Not found!");
        }
        return map;
    }

    @Override
    public Map<String, Object> delete(int id) {
        Map<String,Object> map=new HashMap<>();
        if(articleRepository.delete(id)!=0){
            map.put("Message","Deleted Successfully!");
        }else{
            map.put("Message","Id "+id+" not found!");
        }
        return map;
    }

    @Override
    public Map<String, Object> save(Article article) {
        Map<String, Object> map = new HashMap<>();
        if (categoryRepository.getOne(article.getCategory().getId()) != null) {
            map.put("Message", "Save Successfully!");
            article.setCategory(categoryRepository.getOne(article.getCategory().getId()));
            map.put("data", articleRepository.save(article));
        } else {
            map.put("Message", "Category id " + article.getCategory().getId() + " Not fond!");
        }
        return map;
    }
}
