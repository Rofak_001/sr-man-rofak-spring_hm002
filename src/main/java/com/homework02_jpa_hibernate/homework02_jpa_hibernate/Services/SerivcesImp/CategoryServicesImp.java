package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.SerivcesImp;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Repository.CategoryRepository;
import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services.CategoryServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CategoryServicesImp implements CategoryServices {
    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category getOne(int id) {
        return categoryRepository.getOne(id);
    }

    @Override
    public Map<String,Object> getAll() {
        Map<String,Object> map=new HashMap<>();
        List<Category> list=categoryRepository.getAll();
        if(!list.isEmpty()){
            map.put("data",list);

        }else{
            map.put("Message","No Data!");
        }
        return map;
    }

    @Override
    public Map<String,Object> update(Category category, int id) {
        Map<String,Object> map=new HashMap<>();
        if(categoryRepository.update(category,id)!=0){
            map.put("Message","Updated Successfully");
            map.put("data",categoryRepository.getOne(id));
        }else{
            map.put("Message","Id "+id+" not found!");
        }
        return map;
    }

    @Override
    public  Map<String,Object> delete(int id) {
        Map<String,Object> map=new HashMap<>();
        if(categoryRepository.deleted(id)!=0){
            map.put("Message","deleted Successfully!");
        }else{
            map.put("Message","Id "+id+" not found!");
        }
        return map;
    }

    @Override
    public void save(Category category) {
        categoryRepository.save(category);
    }
}
