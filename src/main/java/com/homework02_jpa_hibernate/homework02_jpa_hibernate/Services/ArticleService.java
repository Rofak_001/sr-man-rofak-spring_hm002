package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Article;

import java.util.List;
import java.util.Map;

public interface ArticleService {
    Map<String, Object> getAll();
    Map<String, Object> getArticleByCategoryTitle(String title);
    Map<String,Object> getOne(int id);
    Map<String, Object> update(Article article,int id);
    Map<String, Object> delete(int id);
    Map<String,Object> save(Article article);
}
