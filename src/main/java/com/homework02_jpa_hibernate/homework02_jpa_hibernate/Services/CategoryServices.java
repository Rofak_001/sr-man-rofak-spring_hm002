package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Services;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;

import java.util.List;
import java.util.Map;

public interface CategoryServices {
    Category getOne(int id);
    Map<String,Object> getAll();
    Map<String,Object> update(Category category, int id);
    Map<String,Object> delete(int id);
    void save(Category category);
}
