package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model;

import javax.persistence.*;

@Entity
@Table(name = "tb_articles")
public class Article {
    @Id
    @GeneratedValue
    private int id;
    private String author;
    private String description;
    private String title;
    @ManyToOne
    private Category category;
    public Article(){}

    public Article( String author, String description, String title, Category category) {
        this.author = author;
        this.description = description;
        this.title = title;
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }
}
