package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Repository;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Article;
import org.apache.tomcat.util.ExceptionUtils;

import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ArticleRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Article> getAll() {
        TypedQuery<Article> query = entityManager.createQuery("select a from Article  a", Article.class);
        return query.getResultList();
    }

    public Article getOne(int id) {
        try {
            TypedQuery<Article> query = entityManager.createQuery("SELECT a FROM Article a WHERE a.id=:id", Article.class);
            query.setParameter("id", id);
            return query.getSingleResult();
        } catch (NoResultException exception) {
            return null;
        }
    }

    public Article save(Article article) {
        entityManager.persist(article);
        return article;
    }

    public List<Article> getArticleByCategoryTitle(String categoryTitle) {
        TypedQuery<Article> query = entityManager.createQuery("SELECT a FROM Article a  Join Fetch a.category c Where lower(c.title) like lower(concat('%',:categoryTitle,'%'))", Article.class);
        query.setParameter("categoryTitle","%"+categoryTitle+"%");
        return query.getResultList();
    }
    public int Update(Article article,int id){
        CriteriaBuilder builder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Article> update=builder.createCriteriaUpdate(Article.class);
        Root<Article> root=update.from(Article.class);
        update.set("author",article.getAuthor());
        update.set("description",article.getDescription());
        update.set("title",article.getTitle());
        update.set("category",article.getCategory());
        update.where(builder.equal(root.get("id"),id));
        Query query=entityManager.createQuery(update);
        return query.executeUpdate();
    }
    public int delete(int id){
        CriteriaBuilder builder=entityManager.getCriteriaBuilder();
        CriteriaDelete<Article> delete=builder.createCriteriaDelete(Article.class);
        Root<Article> root=delete.from(Article.class);
        delete.where(builder.equal(root.get("id"),id));
        Query query=entityManager.createQuery(delete);
        return query.executeUpdate();
    }
}
