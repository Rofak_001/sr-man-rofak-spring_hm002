package com.homework02_jpa_hibernate.homework02_jpa_hibernate.Repository;

import com.homework02_jpa_hibernate.homework02_jpa_hibernate.Model.Category;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CategoryRepository {
    @PersistenceContext
    private EntityManager entityManager;
    public List<Category> getAll(){
        TypedQuery<Category> query=entityManager.createQuery("SELECT c FROM Category c",Category.class);
        return query.getResultList();
    }
    public Category getOne(int id){
        try {
            TypedQuery<Category> query=entityManager.createQuery("SELECT c FROM Category c WHERE c.id=:id",Category.class);
            query.setParameter("id",id);
            return query.getSingleResult();
        }catch (NoResultException exception){
            return null;
        }
    }
    public void save(Category category){
        entityManager.persist(category);
    }
    public int update(Category category,int id){
        CriteriaBuilder builder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Category> update=builder.createCriteriaUpdate(Category.class);
        Root<Category> root=update.from(Category.class);
        update.set("title",category.getTitle());
        update.where(builder.equal(root.get("id"),id));
        Query query=entityManager.createQuery(update);
        return  query.executeUpdate();
    }
    public int deleted(int id){
        CriteriaBuilder builder=entityManager.getCriteriaBuilder();
        CriteriaDelete<Category> delete=builder.createCriteriaDelete(Category.class);
        Root<Category> root=delete.from(Category.class);
        delete.where(builder.equal(root.get("id"),id));
        Query query=entityManager.createQuery(delete);
        return query.executeUpdate();
    }
}
