package com.homework02_jpa_hibernate.homework02_jpa_hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework02JpaHibernateApplication {

    public static void main(String[] args) {
        SpringApplication.run(Homework02JpaHibernateApplication.class, args);
    }

}
